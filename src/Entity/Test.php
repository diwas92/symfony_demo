<?php

namespace App\Entity;

use App\Repository\TestRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TestRepository::class)
 */
class Test
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $teat1;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTeat1(): ?string
    {
        return $this->teat1;
    }

    public function setTeat1(string $teat1): self
    {
        $this->teat1 = $teat1;

        return $this;
    }
}
