<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
class EditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,['attr'=>['autocomplete'=>'off']])
            ->add('username',TextType::class,['attr'=>['autocomplete'=>'off']])
            ->add('phone_number',TextType::class,['attr'=>['autocomplete'=>'off']])
            ->add('edit', SubmitType::class,[
                'attr'=>[
                    'class'=>'btn btn-success',
                ]
            ])

        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}