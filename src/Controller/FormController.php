<?php

namespace App\Controller;
use App\Entity\Form;
use App\Repository\FormRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\FormType;
use App\Form\EditType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class FormController extends AbstractController
{


  // #[Route('/form', name: 'form')]
/**
* @Route("/" ,"name=form" )
*
*/
    public function new(Request $request,ValidatorInterface $validator): Response
    {


        $data = new Form();

        $form = $this->createForm(FormType::class,$data);
        $form->handleRequest($request);

        if ($form->isSubmitted()&&$form->isValid()){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($data);
            $entityManager->flush();
            //return $this->redirectToRoute('/form');
            //return $this->redirect('/form/users');

        }

        return $this->render('form/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }



    /**
     * @Route("/form/users" , name="formusers")
     *
     */


    public function show(Request $request)
    {
        $data = new Form();
        $form = $this->createForm(EditType::class,$data);

        $infos = $this->getDoctrine()->getRepository(Form::class)->findAll();

        if (!$request->isXmlHttpRequest())
        {
            return $this->render('form/view.html.twig',[
                'info'=>$infos,
                'form'=>$form->createView(),
            ]);

        }
        else{
             $x=0;

            $data = array();
            foreach($infos as $info)
            {
             $temp = array(
                 'name'=>$info->getName(),
                 'username'=>$info->getUsername(),
                 'phone_number'=>$info->getPhoneNumber()
             );
             $data[$x++] = $temp;
            }

            return new JsonResponse($data);
        }
    }



    /**
     * @Route("/info/{id}")
     */

    public function showDetails(Request $request,$id, FormRepository $formRepository)
    {

       // return new JsonResponse($id);

     $infos = $formRepository->find($id);

     if($request->isXmlHttpRequest())
     {

         return $this->render('form/showDetails.html.twig',['info'=>$infos,]);
     }
     else{

             $temp = array();

                 $temp['name']=$infos->getName();
                 $temp['username']=$infos->getUsername();
                 $temp['phone_number']=$infos->getPhoneNumber();

             return new JsonResponse($temp);
    }
    }
    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function edit(Request $request,$id): Response
    {

         $my_val=$request->get('edit');
        //$data = new Form();
        $datas = $this->getDoctrine()->getRepository(Form::class)->find($id);
        $form = $this->createForm(EditType::class,$datas);
        $form->handleRequest($request);
        if ($form->isSubmitted()&&$form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $datas->setName($my_val['name']);
            $datas->setUsername($my_val['username']);
            $datas->setPhoneNumber($my_val['phone_number']);


            $entityManager->flush();

            //return $this->redirectToRoute('/form');
             //return $this->redirect('/form/users');
        }


         return $this->render('form/edit.html.twig', [
             'form' => $form->createView(),
         ]);
    }


    /**
     * @Route("/delete/{id}")
     * Method{{DELETE}}
     */
    public function delete(Request $request,$id)
    {
        $entityManger = $this->getDoctrine()->getManager();
        $data = $this->getDoctrine()->getRepository(Form::class)->find($id);
        $entityManger->remove($data);
        $entityManger->flush();
        return $this->redirect('/form/users');
//         $response = new Response();
//         $response->send();
    }

}
