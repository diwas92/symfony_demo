<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Product;
use App\Repository\ProductRepository;

use Doctrine\ORM\EntityManagerInterface;

class ProductController extends AbstractController
{
    #[Route('/product/delete/{id}', name: 'product')]
// ****************insert into database***************


//    public function index(): Response
//    {
//        $entityManager = $this->getDoctrine()->getManager();
//        $product = new Product();
//        $product->setAddress('kathmandu');
//        $entityManager->persist($product);
//        $entityManager->flush();
//
////        return $this->render('product/index.html.twig', [
////            'controller_name' => 'ProductController',
////        ]);
//
//        return new Response('Saved new product with id '.$product->getId());
//    }


    //************ fetched from database ************


//    public function show(int $id): Response
//    {
//        $product = $this->getDoctrine()
//            ->getRepository(Product::class)
//            ->find($id);
//
//        if (!$product) {
//            throw $this->createNotFoundException(
//                'No product found for id '.$id
//            );
//        }
//
//        return new Response('Check out this great product: '.$product->getAddress());

    //***********  update data in database  ****************


// public function update(int $id): Response
//    {
//        $entityManager = $this->getDoctrine()->getManager();
//        $product = $entityManager->getRepository(Product::class)->find($id);
//        if (!$product)
//        {
//            throw $this->createNotFoundException('no product found for id ' .$id);
//
//        }
//        $product->setAddress('pokhara 16');
//        $entityManager->flush();
//        return new Response('updated id is:'.$product->getId());
//
//    }
//*********************delete from database******************
public function delete(int $id): Response
{
    $entityManager =$this->getDoctrine()->getManager();
    $product = $entityManager->getRepository(Product::class)->find($id);
    $entityManager->remove($product);
    $entityManager->flush();
    return new Response('deleted address is:'.$product->getAddress());
}

}
