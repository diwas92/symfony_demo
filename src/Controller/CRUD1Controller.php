<?php

namespace App\Controller;

use App\Entity\CRUD;
use App\Form\CRUDType;
use App\Repository\CRUDRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/c/r/u/d1')]
class CRUD1Controller extends AbstractController
{
    #[Route('/', name: 'c_r_u_d1_index', methods: ['GET'])]
    public function index(CRUDRepository $cRUDRepository): Response
    {
        return $this->render('crud1/index.html.twig', [
            'c_r_u_ds' => $cRUDRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'c_r_u_d1_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $cRUD = new CRUD();
        $form = $this->createForm(CRUDType::class, $cRUD);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($cRUD);
            $entityManager->flush();

            return $this->redirectToRoute('c_r_u_d1_index');
        }

        return $this->render('crud1/new.html.twig', [
            'c_r_u_d' => $cRUD,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'c_r_u_d1_show', methods: ['GET'])]
    public function show(CRUD $cRUD): Response
    {
        return $this->render('crud1/show.html.twig', [
            'c_r_u_d' => $cRUD,
        ]);
    }

    #[Route('/{id}/edit', name: 'c_r_u_d1_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, CRUD $cRUD): Response
    {
        $form = $this->createForm(CRUDType::class, $cRUD);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('c_r_u_d1_index');
        }

        return $this->render('crud1/edit.html.twig', [
            'c_r_u_d' => $cRUD,
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'c_r_u_d1_delete', methods: ['POST'])]
    public function delete(Request $request, CRUD $cRUD): Response
    {
        if ($this->isCsrfTokenValid('delete'.$cRUD->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($cRUD);
            $entityManager->flush();
        }

        return $this->redirectToRoute('c_r_u_d1_index');
    }
}
