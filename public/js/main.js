const table = document.getElementById('table');
if (table)
{
    table.onclick = function(e)
    {
        if (e.target.className === 'btn btn-danger delete-user' )
        {
            if(confirm('Are you sure you want to delete?'))
            {
                const id = e.target.getAttribute('data-id');
                fetch(`/delete/${id}`,{method: 'DELETE'}).then(res=>window.location.reload())
            }
        }
    }
}
